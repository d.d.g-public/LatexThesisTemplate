# Thesis CWL File
# Daniel.dg 14/10/2021

# -----------------
# Environments
# -----------------
\begin{comment}
\end{comment}
\begin{abbreviations}
\end{abbreviations}
\begin{abstract}
\end{abstract}
\begin{acknowledgements}
\end{acknowledgements}
\begin{declaration}
\end{declaration}
\begin{constants}
\end{constants}
\begin{symbols}
\end{symbols}

# -----------------
# Document Commands
# -----------------
# Title Page
\inserttitlepage
\setthesistitle{shorttitle%text}{title%text}
\setauthor{author%text}
\setsupervisor{supervisor%text}
\setuniversity{university%text}
\setdepartment{department%text}
\setpostgradtype{gradtype%text}
\setfaculty{faculty%text}
\setkeywords{keywords%text}

\shorttitle
\thesistitle
\authorname
\supervisorname
\universityname
\departmentname
\graduationtype
\facultyname
\keywords

# Quote Page
\insertquotepage
\quotetext{quotation%text}
\quoteauth{author%text}

# Chapters
\includechapter{file}#i

# Appendicies
\includeappendix{file}#i

# Math
\symunit{glsRef%text}#m
\symunitns{glsRef%text}#m
\symvalunit{glsRef%text}#m
\customunit{unit%text}#m
\customunitns{unit%text}#m
\SIunit{siUnit%text}#m
\SIunitf{siUnit%text}#m

\subscript{subscript%text}
\superscript{superscript%text}


# Tables
\multilinecell{content%text}#t

# Figures
\source{figsource%text}#/figure
# \insertcenteredfigure[position%text]{size%text}{figname%imagefile}{caption%text}<figsource%text>{label%labeldef}
\insertsubfigure[size%text]{figname%imagefile}{label%labeldef}<caption%text>
\insertfigure[size%text]{filename%imagefile}#g/figure

# Text
\concern{text%text}

# Referencing
\figref{figure%ref}
# \figrefpart{figure%ref}{part%text}
\tabref{table%ref}
\eqnref{equation%ref}
\appref{appendix%ref}
\secref{section%ref}
\chapref{chapter%ref}
\partref{part%ref}

\missingcitation{citation%text}

# Glossaries
\importsymbols{bibfile%text}
\importsubsuper{bibfile%text}
\importsubscripts{bibfile%text}
\importacronyms{bibfile%text}
\importglossary{bibfile%text}
\insertnomenclature

\glsentryunit{glsRef%ref}
\Glsentryunit{glsRef%ref}
\glsunit{glsRef%ref}
\Glsunit{glsRef%ref}
\GLSunit{glsRef%ref}
\glsentryvalue{glsRef%ref}
\GLsentryvalue{glsRef%ref}
\glsvalue{glsRef%ref}
\Glsvalue{glsRef%ref}
\GLSvalue{glsRef%ref}

# TODO Notes
\info{note%text}
\unsure{note%text}}
\change{note%text}}
\improvement{note%text}}
\infof{note%text}
\unsuref{note%text}}
\changef{note%text}}
\improvementf{note%text}}

# Displaying latex code
\displaycoderaw{code%text}#\math
\displaycode{code%text}#\math

# Dedicatory
\dedicatory{dedicatedto%text}#S
\dedicatory{dedicatedto%text}[optional%text]#S


# -----------------
# Class Commands
# -----------------
\singlespace#S
\captionsenglish#S
\shorttitle#S
\PreventPackageFromLoading#S
\MessageBreak#S
\symbolsname#S
\todotocname#S
\PreserveBackslash#S
\checktoopen
\removespaces#S
\autospace#S
\blankpagestyle#S
\setblankpagestyle#S
\setchapterpagestyle#S
\abbrevname#S
\abstractname#S
\byname#S
\acknowledgementname#S
\authorshipname#S
\nomenclaturename#S
\constantsname#S
\symbolsname#S
\todotocname#S
\todotoc#S


# Glossaries
\glsaddkey#S
\setabbreviationstyle#S
\GlsXtrEnableInitialTagging#S
\glssetcategoryattribute#S
\itag#S
