\chapter{The Class File Explained}

\label{Sections}

%----------------------------------------------------------------------------------------
%	PREAMBLE
%----------------------------------------------------------------------------------------
This thesis class was created to reduce the amount of work required to write a nicely formatted thesis. This class template includes some useful features that improve the overall outcome of the thesis, whilst also improving productivity during the writing stages. Some of these features are:
\begin{itemize}
	\item Automatic and consistent layout of the chapters and other parts of the document	
	\item Hyper-linked Table of Contents, Table of Figures, and Table of Tables
	\item Nomenclature including:
		\begin{itemize}
			\item Hyper-linked items
			\item Items sorted into Symbols, Abbreviations, Subscripts, Superscripts and Glossary
			\item Items can be sorted and included using .bib files (enables neat management)
		\end{itemize}
	\item TODO notes
	\item Interface commands to simplify the document and reduce the amount of repeated commands
\end{itemize}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Prerequisites}
	
	There are some important prerequisites for this template to work correctly in Tex Studio. It is recommended to be using a MikTex install with Tex Studio as the editor. 
	
	%----------------------------------------------------------------------------------------
	%	SUBSECTION 1
	%----------------------------------------------------------------------------------------
	\subsection{GhostScript}
		
		GhostScript is an application that does not get installed with Latex installs but can be used by several functions in Latex. In this template it is extremely useful for importing eps figures into the document. An exe installer has been placed in this repository. It is recommended to install this.
	
	%----------------------------------------------------------------------------------------
	%	SUBSECTION 2
	%----------------------------------------------------------------------------------------
	\subsection{Java}
	
		Java must be installed for this template to work correctly. More specifically, some of the biber and glossaries commands rely on Java.
	
	%----------------------------------------------------------------------------------------
	%	SUBSECTION 3
	%----------------------------------------------------------------------------------------
	\subsection{Default Settings}
		
		The settings of Tex Studio must be set correctly for this template to work correctly. There are profile templates in this repository that can be loaded in to do this automatically. The dark mode template (Normal - Dark Mode.txsprofile) is recommended. The templates can be loaded through:\\
		\displaycoderaw{Options -> Load Profile...}\\
		
		Note: The grouping templates are untested. These are intended for grouping of nomenclature items.
		
	%----------------------------------------------------------------------------------------
	%	SUBSECTION 4
	%----------------------------------------------------------------------------------------
	\subsection{CWL Files}
		
		This thesis template contains several custom commands and are thus unrecognised by Tex Studio. This results in command syntax that is highlighted throughout the document. A simple solution to this is to provide a CWL file for Tex Studio so that it recognises what the commands are ans what they do. This is purely for visually correcting the code in the Tex Studio editor and has no impact on the outcome of the document. The CWL file in this repository should be copied into the following folder on a windows system:
		
		\qquad\qquad \%appdata\%\textbackslash texstudio\textbackslash completion\textbackslash user
		
		The CWL files need to be activated in the Tex Studio Configuration settings under the Completion heading. If the Thesis.cwl file is placed in the correct location, it should appear in the list of cwl files (near the end). Enable it and click Okay to exit the configuration window.
	
	%----------------------------------------------------------------------------------------
	%	SUBSECTION 5
	%----------------------------------------------------------------------------------------
	\subsection{Zotero}
		While this is not a strict requirement, it is a highly recommended software for managing and importing references into the document. The tool requires a small amount of setup. The first change is to change the export format in the preferences to 'IEEE'. Next it is recommended to set up syncing such that you have backups of the references, or if you would like syncing across multiple computers. 
		
		To use this tool, you first must add the references into the software, typically using an extension installed in your browser. To export the references to your document, you will need to do the following:
		\begin{enumerate}
			\item Select the appropriate references
			\item Right click
			\item Select 'Export Items...'
			\item Change the format to 'BibLaTeX'
			\item Press 'Okay'
			\item Create and export to a temporary .bib file (you can use an existing temporary .bib file)
			\item Open the temporary . bib file and copy the contents over to your main .bib file for references
		\end{enumerate}
		
		Download: \url{https://www.zotero.org/}
		
%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------
\section{Class Arguments}
	The class file contains a set of arguments that can be called during the setup process of the document. This enables the document to be visually and functionally configured differently. \tabref{Tab:ClassCommands} outlines the arguments for the class and their descriptions.
	
	
	\begin{table}[ht!]
		\begin{center}
			\caption{The class argument definitions.}
			\begin{tabularx}{0.99\textwidth} { 
					>{\raggedright\arraybackslash\hsize=0.25\hsize}X 
					>{\raggedright\arraybackslash\hsize=0.75\hsize}X  }
				\toprule
				\textbf{Argument} & \textbf{Definition}\\
				\midrule
				11pt					& Sets the size of the body text. Alternatively, 10pt or 12pt can be used. \\
				oneside 				& Enables oneside mode for the document. Default is two side (alternating margins) for binding. \\
				english  				& Sets the language to english. Alternatively, language can be changed (untested) \\
				doublespacing  			& Line spacing. Alternatively, onehalfspacing or singlespacing can be used. \\
				finalversion 			& Sets the document to final version, automatically calls disabletodo argument. \\
				nolistspacing 			& Sets spacing in lists to single. Useful if onehalfspacing and doublespacing is used. \\
				liststotoc 				& Adds the list of figures/tables/etc to the table of contents. \\
				toctotoc 				& Adds the main table of contents to the table of contents. \\
				parskip 				& Adds space between paragraphs. \\
				nohyperref 				& Disables hyper-links. \\
				headsepline 			& Adds a line under the header. \\
				chapterinoneline 		& Adds the chapter title next to the number on one line. \\
				consistentlayout 		& Changes the layout of the declaration, abstract and acknowledgements pages to match the default layout. \\
				disabletodo 			& Disables the ToDo notes. \\
				enablenomenclat urevalue & Enables the value column in the nomenclature table. \\
				resetcountonpart 		& Resets the chapter number in each part of the thesis. (Not Recommended) \\
				\bottomrule
			\end{tabularx}
			\label{Tab:ClassCommands}
		\end{center}
	\end{table}

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{Compiling the Document}
	
	This template requires a specific set of commands that compile it in the correct manner such that it is displayed correctly with no errors. The Tex Studio setting profiles (included with the template) implement the correct set of commands. A full or partial compile can be used to compile the document.
	
	\subsection{Full Compile}
		A full compile will recompile the entire thesis document including reloading all .bib files for the Nomenclature and References. This is extremely useful for compiling the document for the first time, and for when any of the .bib files are modified. If the .bib files are modified and a full recompile is not performed, the latex compiler will not see these changes and you will have errors in the resulting document (Latex will not warn you of these errors).
		
		To perform a full compile you can press the green \textbf{fast-forward} button in the toolbar or the keyboard short-cut \textbf{F5}.
		
	\subsection{Partial Compile} 
		A partial compile will only recompile the .tex files in the document. This is extremely useful for quickly compiling the document to update changes that have been performed inside of the .tex files (e.g. adding words, figures or tables).
		
		To perform a partial compile you can press the green \textbf{play} button in the toolbar or the keyboard short-cut \textbf{F6}.
		
	\subsection{Setting the Root Document}
		By default, Tex Studio will compile the document using the current .tex file that is open or active in the window. This will result in errors if the document is compiled from within any of the .tex files that are not the main.tex file. Therefore, requiring you to switch back to the main .tex file every time you need to compile the document. 
		
		Setting the root document can speed up the writing and compiling process because it enables you to compile the document from anywhere inside the project. E.g. you can recompile the document from within one of the chapter.tex files. To set this up, open the main .tex file and then navigate to:\\
		\displaycoderaw{Options -> Root Document -> Set Current Document As Explicit Root}\\
		
%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------
\section{Modularity}
	
	This thesis template has been set up to encourage a modular approach for writing a thesis. Each chapter, appendix and figure is stored as a single file in its respective folder. Figures will be explained in \chapref{Images}.
	
	If parts are required, they can be created through the standard command:\\
	\displaycoderaw{\part{part title}}\\
	
	Chapters are located in the Chapter folder where it is called in the main thesis tex file using the following command:\\
	\displaycoderaw{\includechapter{filename}}\\
	
	Appendices are located in the Appendices folder where it is also called in the main thesis tex file using the following command:\\
	\displaycoderaw{\includeappendix{filename}}\\
	
%----------------------------------------------------------------------------------------
%	SECTION 5
%----------------------------------------------------------------------------------------
\section{Suggested Layout}
	
	Each chapter in your thesis will likely contain some sections and subsections. It is highly recommended that you do not go deeper in level than a subsection (e.g. a subsubsection) because this can become messy and difficult to follow. A chapter will typically start with some preamble that introduces or starts off the chapter. The template chapters have been set out such that there is a dedicated preamble section if it is required. 
	
	All sections and subsections will follow after this. A section and subsection are created using the following code:\\
	\displaycoderaw{\section{section title}}\\
	\displaycoderaw{\subsection{subsection title}}\\
	
	A highly recommended practise is to implement tabbing into layout of .tex files. This does not effect the output of the document, but it significantly improves the readability of each .tex file. There are also comments in the template files that help signify where sections and subsections start.
	
%----------------------------------------------------------------------------------------
%	SECTION 6
%----------------------------------------------------------------------------------------
\section{Compressing The Document}
	
	It is very common for a thesis to have a large file size, greater than 100mb, especially when high quality images have been included. While this can produce a high quality thesis that visually looks good, it can make it difficult for submitting the thesis online (20mb upload limit for UoC Turnitin) or sharing it via email. In this case the final version of the thesis should be compressed such that it is less than 20mb in size. There are two common methods for this.
	
	%----------------------------------------------------------------------------------------
	%	SUBSECTION 1
	%----------------------------------------------------------------------------------------
	\subsection{Method 1 (Recommended)}
		
		Once the thesis has been compiled, a script can be run to generate a compressed version of the thesis. For your convenience, a shell script has also been created to simplify the process. These scripts requires GhostScript to be installed and a bash terminal (typically installed with Git). The required script has been placed in the root directory of this repository with the name:
		\displaycoderaw{CompressThesis.sh}.
		All you need to do is execute this script. Alternatively, there is also a batch file that calls the shell script if that is of any use to some users.
		
		The shell script runs a script called optpdf which contains all of the commands for compressing the pdf. If you find the script is not optimised for your thesis, e.g. compression ratio is not enough, then you can modify this file in a text editor. Line 12 is of interest and the \displaycoderaw{-dPDFSETTINGS=} command is responsible for the compression level. You will need to do some googling to figure out the rest because this is not a necessary step for most users.
		
	%----------------------------------------------------------------------------------------
	%	SUBSECTION 1
	%----------------------------------------------------------------------------------------
	\subsection{Method 2 (Not Recommended)}
		
		This method is less likely to produce good consistent results between different theses. Reducing the images prior to compiling the thesis can significantly reduce reduce the file size. However, this requires a lot of effort if this is not done throughout the thesis writing process. InkScape is capable of reducing image file sizes. 
		
		
		
		