% Nomenclature

\chapter{Nomenclature}

\label{Nomenclature}

%----------------------------------------------------------------------------------------
%	PREAMBLE
%----------------------------------------------------------------------------------------

The glossaries-extra package has been used to enable easy use of the Nomenclature. All items of the Nomenclature are stored in one or more .bib files to help sort them such that they are imported correctly.


%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Adding Nomenclature Items}
	%----------------------------------------------------------------------------------------
	%	SUBSECTION 1
	%----------------------------------------------------------------------------------------
	\subsection{Adding Symbols}
		Symbols are added using the following \displaycoderaw{@symbol{...}} command in a .bib file. The symbol, description, value, unit and label are then put inside of the brackets with comma separation.
		\begin{itemize}
			\item The label should be inserted first. This is used for referencing the symbol.
			\item The symbol name is set by \displaycoderaw{name = {\ensuremath{...}}}
			\item The symbol value is set by \displaycoderaw{value = {...}}
			\item The symbol unit is set by \displaycoderaw{name = {\si{...}}}
			\item The symbol description is set by \displaycoderaw{description = {...}}
		\end{itemize}
	
		The SI unit package has been used and is there to ensure the correctness of units. It is highly recommended that you use it.
		
		\noindent\textbf{NOTE:} Do \textbf{not} leave trailing spaces between the argument value and the closing bracket, \}. This will cause the glossaries package to break.
		
	%----------------------------------------------------------------------------------------
	%	SUBSECTION 2
	%----------------------------------------------------------------------------------------
	\subsection{Adding Abbreviations}
		Abbreviations are added using the following \displaycoderaw{@acronym{...}} command in a .bib file. The abbreviations can then be defined by the following commands:
		\begin{itemize}
			\item The label should be inserted first. This is used for referencing the abbreviation.
			\item The long form is set by \displaycoderaw{long = {...}}
			\item The short form is set by \displaycoderaw{short = {...}}
			\item The category is set by \displaycoderaw{category = {...}} (typically 'standard')
		\end{itemize}
		
		The glossaries package assumes the plural form of the short name is defined by a trailing s. If this is not the case, then the plural short form can be changed through \displaycoderaw{plural = {...}}.
		
		This template has been set up such that dot separated abbreviations can be used. If this is required then the category must be set to 'dottedsc' so that the package can automatically insert dots between each character of the short form. Additionally, the \displaycoderaw{\itag{...}} command can be used to signify the characters in the long form that correspond to the abbreviated characters (NOTE: this does not show up anywhere in the document, only the Nomenclature table).
		
		For example, \gls{HTML} uses the 'dottedsc' category with a long description of \underline{H}yper\underline{T}ext \underline{M}arkup \underline{L}anguage in the abbreviation list.
		
		\noindent\textbf{NOTE:} Do \textbf{not} leave trailing spaces between the argument value and the closing bracket, \}. This will cause the glossaries package to break.
		
	%----------------------------------------------------------------------------------------
	%	SUBSECTION 3
	%----------------------------------------------------------------------------------------
	\subsection{Adding Glossaries}
		Glossary items are added using the standard \displaycoderaw{@entry{...}} command in a .bib file. The glossary items can then be defined by the following commands:
		\begin{itemize}
			\item The label should be inserted first. This is used for referencing the abbreviation.
			\item The name is set by \displaycoderaw{name = {...}}
			\item The description is set by \displaycoderaw{description = {...}}
		\end{itemize}
		
		\noindent\textbf{NOTE:} Do \textbf{not} leave trailing spaces between the argument value and the closing bracket, \}. This will cause the glossaries package to break.
		
%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------
\section{Referencing Symbols}
	\subsection{Referencing Symbols with a hyperlink}
		Use the \displaycoderaw{\gls{...}} command.
		
		Symbols: $\gls{x}$, $\gls{u}$, $\gls{a}$, $\gls{t}$, $\gls{F}$ and $\gls{alpha}$.
		
	\subsection{Referencing Symbols without a hyperlink}
		Use the \displaycoderaw{{\glsdisablehyper \gls{...}}} command.
	
		Symbols: {\glsdisablehyper $\gls{x}$, $\gls{u}$, $\gls{a}$, $\gls{t}$, $\gls{F}$ and $\gls{alpha}$}.
	
	\subsection{Referencing Symbol Values with a hyperlink}
		Use the \displaycoderaw{\glsvalue{...}} command.
	
		The value of $\gls{alpha}$ is $\glsvalue{alpha}$.
	
	\subsection{Referencing Symbol Values without a hyperlink}
		Use the \displaycoderaw{\glsentryvalue{...}} command.
	
		The value of $\gls{alpha}$ is $\glsentryvalue{alpha}$.
		
	\subsection{Referencing Symbol Units with a hyperlink}
		Use the \displaycoderaw{\glsunit{...}} command.
	
		The unit for $\gls{alpha}$ is $\glsunit{alpha}$.
		
	\subsection{Referencing Symbol Units without a hyperlink}
		Use the \displaycoderaw{\glsentryunit{...}} command.
	
		The unit for $\gls{alpha}$ is $\glsentryunit{alpha}$.
		
	\subsection{Using Symbols in Math Mode}
		Be careful when you use glossary entries in math mode because it is easy to sun into issues with consecutive hyper-links. For example, errors can arise if one symbol has a subscript and superscript containing another symbol. The following equation is an example of this:\\
		\displaycoderaw{$\gls{F}_\gls{drag}$} \\
		which should produce $\gls{F}\subscript{drag}$ where $\gls{F}$ and $\gls{drag}$ are hyper-linked correctly.
		
		\noindent In this case, the following commands have been created to fix this issue:\\
		\displaycoderaw{\subscript{...}}\\
		\displaycoderaw{\superscript{...}}
		
		\noindent Therefore, \displaycoderaw{$\gls{F}_\gls{drag}$} should be rewritten as:\\
		\displaycoderaw{$\gls{F}\subscript{drag}$}
		
		\vspace{0.5cm}
		
		\noindent In some scenarios you may want additional characters, subscripts or superscripts to be included in the hyper-linked result. For example, suppose you have the following code:
		
		\displaycode{\gls{F}^2}
		
		\noindent This will result in the superscript having no associated hyperlink. In this situation it’s best to bring the superscript into the hyperlink using the final 'hinserti' optional argument:
		
		\displaycode{\gls{F}[^2]}

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{Referencing Abbreviations}
	\subsection{First use of abbreviations}
		The first use of abbreviations are in the form: long (short)
		
		\gls{TTL}
		
		\gls{GNU}
	
	\subsection{Subsequent uses of abbreviations}
	
		All subsequent uses of abbreviations are in the form: short
		
		\gls{TTL}
		
		\gls{GNU}
		
	\subsection{Altering the First Use Flag}
		Sometimes it may be desirable to jump straight to the short form for the first of an abbreviation, or it may be necessary to have the first use form listed more than once in the document. This can all be achieved by altering the first use flag of each or all of the abbreviations. 
		
		To use the short form on first use the \displaycoderaw{\glsunset{label}} command should be used for each abbreviation prior to its use. Otherwise, \displaycoderaw{\glsunsetall} can be used to reset all variables.
		
		To get the first use form again, the \displaycoderaw{\glsreset{label}} command should be used for each abbreviation prior to its use. Otherwise, \displaycoderaw{\glsresetall} can be used to reset all variables.
		
		
	\subsection{Abbreviation Plural Form}
	
		The plural abbreviated from of \gls{TTL} is accessed by \displaycode{\glspl{TTL}}. 
	
	\subsection{Accessing the long form of an abbreviation}
		
		The long form of \gls{TTL} can be accessed by \displaycode{\glsentrylong{TTL}} but it is not hyper-linked.
		
		Instead use the shortcut \displaycode{\acl{TTL}}

	\subsection{Glossaries-Extra Shortcut Commands}		
		Sometimes it may be desirable to use the abbreviations without effecting the first use flag, or it may be desirable to get the exact form (long or short) of an abbreviation. In these cases, shortcut commands should be used. If you require any of these commands (other than the ones listed below) consult the documentation for the glossaries-extra package.
		
		\displaycoderaw{\acs{...}} and \displaycoderaw{\acsp{...}} can be used to access the short and short plural forms respectively.
		
		\displaycoderaw{\acl{...}} and \displaycoderaw{\aclp{...}} can be used to access the long and long plural forms respectively.
		
		
	\subsection{Abbreviations in Headings}
		Caution should be used when inserting abbreviations into the headings for sections, chapters, etc. If the standard commands are used, then the headings will contain hyperlinked abbreviations in the headings and thus the table of contents. This is an issue because if the particular part of the heading is clicked in the table of contents, then it will take you to the Nomenclature instead of the desired section in the document. In this case the following commands should be used to insert abbreviations into headings:\\
		\displaycoderaw{\glsentryshort{...}}\\
		\displaycoderaw{\glsentrylong{...}}\\
		