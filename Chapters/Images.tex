\chapter{Figures} % Main chapter title

\label{Figures}

%----------------------------------------------------------------------------------------
%	PREAMBLE
%----------------------------------------------------------------------------------------

Inkscape is a free software that enables images to be edited easily, and more importantly it supports vector images. Vector images are recommended to ensure they are high quality and can be scaled easily. The most common (and easiest) vector image types are eps and pdf. Alternatively, other non-vector image types, such as JPEG, can be included in a document using the same commands.

Images (aka Floats) are typically inserted into a thesis document as a figure where they are horizontally centred on the page. Latex enables the size and vertical position of the image to be easily defined through some option arguments. 

Download Inkscape: \url{https://inkscape.org/}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Inserting a Figure}
	The figure environment is used to insert an image into the document, as shown below:\\
	\displaycoderaw{\begin{figure}[position]}\\
	\displaycoderaw{...}\\
	\displaycoderaw{\end{figure}}\\
	
	The position argument is optional and cant contain a set of positions that are evaluated sequentially until one is satisfied. The position arguments are as follows:
	\begin{itemize}
		\item h - means place the float here, i.e., approximately at the same point it occurs in the source text (however, not exactly at the spot)
		\item t - means position at the top of the page.
		\item b - means position at the bottom of the page.
		\item p - means put on a special page for images only.
		\item ! - means override internal parameters LATEX uses for determining "good" float positions.
	\end{itemize}
	
	Vector and non-vector images can be included using the following command:\\
	\displaycoderaw{\insertfigure[size]{imagename}}\\
	The size argument is optional but recommended, and can be any value between 0 and 0.99. The function will automatically find the image in the Figures folder. An example of including an image can be seen in the source code, where the result is shown in \figref{Fig:test}. The figure is referenced through the following command:\\
	\displaycoderaw{\figref{Fig:label}}\\
	
	\begin{figure}[htb!]
		\centering
		\insertfigure[0.8]{Electron}
		\caption{Test image.}
		\label{Fig:test}
	\end{figure}
	
	
%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------
\section{Inserting a Figure with Text}
	Sometimes it is desirable to include text in an image. However, it can be a difficult (or iterative) task get the text the correct size (or font) such that it is readable in the document. To overcome this, a type of pdf image, known as pdf\_tex, can be created using a software like Inkscape. Basically, this seperates the text from the image and places it in a separate file (pdf\_tex file). Latex uses this file to obtain the correct placement for the text and treats it as normal text in the document. This means the text can include commands or math. Example code can be seen below:\\
	\displaycoderaw{\insertfigure[size]{imagename.pdf_tex}}\\
	
	An example of this can be seen in \figref{Fig:ImageText}. This figure contains symbols that are used in the Nomenclature. You can see this by clicking on the symbol (it is a hyper-link now). The symbols are inserted the same way they are inserted in the document (\chapref{Nomenclature} explains how to do this in more detail). This can be useful in some scenarios where symbols or abbreviations are used in the figure.
	
	\begin{figure}[htb!]
		\centering
		\insertfigure[0.5]{Free Body Diagram Balloon.pdf_tex}
		\caption{Free Body Diagram of a balloon.}
		\label{Fig:ImageText}
	\end{figure}
	
	NOTE: the text position changes with image scaling due to the font size being independent of image scale. In this case the test will need to be moved, and is easy to do so in Inkscape. 

%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{Inserting a Subfigure}
	Sub-figures can also be included in the document using the subfigure environment inside a figure environment. A simplified command has been exposed for easily including subfigures into the environment, which is:\\
	\displaycoderaw{\insertsubfigure[size]{figname}{label}<caption>}\\
	
	In general, a caption should not be included in subfigures and the main figure caption should explain all subfigures. However, the caption argument has been left in the exposed command for special cases where a caption is required. A sub-caption reference can also be used in the main figure caption for correctness using:\\
	\displaycoderaw{\subref{Fig:label}}\\
	
	\noindent Each subfigure is referenced through the following command:\\
	\displaycoderaw{\figref{Fig:label}}\\
	
	\noindent\textbf{NOTE:} Do not use the subfig package! It is incompatible with this template. 
	
	\subsection{Vertical Subfigures}
		\figref{Fig:TwoFiguresVert} illustrates an example of using subfigures vertically. The vertical spacing, typically '1ex', is set through the following command and should be inserted between each sub figure:\\
		\displaycoderaw{\\[spacing]}
		
		\begin{figure}[htb!]
			\centering
			\insertsubfigure[0.3]{Electron}{Fig:ElectronVert1} 
			\\[1ex]
			\insertsubfigure[0.3]{Electron}{Fig:ElectronVert2}
			\caption{Subfigure example of (\subref{Fig:ElectronVert1}) an electron and (\subref{Fig:ElectronVert2}) another electron.}
			\label{Fig:TwoFiguresVert}
		\end{figure}
	
	\subsection{Horizontal Subfigures}
		Subfigures can be aligned horizontally where there is some freedom over the horizontal spacing. It is important that the total combined width of the subfigures and their horizontal spacing do not exceed the width of the column, otherwise errors will occur in the resulting document.
		
		The most common method for inserting horizontally aligned subfigures is shown in \figref{Fig:Fig:FourFiguresHorEq}. The method for achieving this uses an \displaycoderaw{\hspace*{\fill}} command at the beginning and end of the figure environment, and a \displaycoderaw{\hfill} command between each subfigure. This effectively centres a group of subfigures on the page with equal distance between each subfigure.
		
		\begin{figure}[htb!]
			\centering
			\hspace*{\fill}
			\insertsubfigure[0.1]{Electron}{Fig:ElectronHorEq1} 
			\hfill
			\insertsubfigure[0.1]{Electron}{Fig:ElectronHorEq2}
			\hfill
			\insertsubfigure[0.1]{Electron}{Fig:ElectronHorEq3}
			\hfill
			\insertsubfigure[0.1]{Electron}{Fig:ElectronHorEq4}
			\hspace*{\fill}
			\caption{Subfigure example of (\subref{Fig:ElectronHorEq1}) an electron and (\subref{Fig:ElectronHorEq2}) ...}
			\label{Fig:FourFiguresHorEq}
		\end{figure}
		
		Alternatively, a group of subfigures can be placed centred on the page with a varying distance between each subfigure, as shown in \figref{Fig:ThreeFiguresHorUD}. Unlike the first method, the only command required is a \displaycoderaw{\hspace{spacing}} between each subfigure, where spacing is a user defined value like '1cm'.
		
		\begin{figure}[htb!]
			\centering
			\insertsubfigure[0.2]{Electron}{Fig:ElectronHorUD1} 
			\hspace{0.5cm}
			\insertsubfigure[0.2]{Electron}{Fig:ElectronHorUD2}
			\hspace{2.5cm}
			\insertsubfigure[0.2]{Electron}{Fig:ElectronHorUD3}
			\caption{Subfigure example of (\subref{Fig:ElectronHorUD1}) an electron and (\subref{Fig:ElectronHorUD2}) ...}
			\label{Fig:ThreeFiguresHorUD}
		\end{figure}
		
		Subfigures can also be aligned such that they are placed at the outer edges of the page. The following figure illustrates this:
		
		\begin{figure}[htb!]
			\centering
			\insertsubfigure[0.2]{Electron}{Fig:ElectronHorEA1} 
			\hfill
			\insertsubfigure[0.2]{Electron}{Fig:ElectronHorEA2}
			\caption{Subfigure example of (\subref{Fig:ElectronHorEA1}) an electron and (\subref{Fig:ElectronHorEA2}) ...}
			\label{Fig:TwoFiguresHorEA}
		\end{figure}
		
		
	\newpage{}
	\subsection{Combination of Subfigure Positions}
		Sometimes you may want a combination of subfigures aligned horizontally and vertically from one another. \figref{Fig:FourFiguresGrid} illustrates an example of using four subfigures in 2x2 grid. This uses the horizontal and vertical subfigure placement techniques, where the figures are inserted row-by-row.
		
		\begin{figure}[htb!]
			\centering
			\hspace*{\fill}
			\insertsubfigure[0.2]{Electron}{Fig:ElectronGridTL} % Top Left
			\hfill
			\insertsubfigure[0.2]{Electron}{Fig:ElectronGridTR} % Top Right
			\hspace*{\fill}
			\\[1ex] % New Row
			\hspace*{\fill}
			\insertsubfigure[0.2]{Electron}{Fig:ElectronGridBL} % Bottom Left
			\hfill
			\insertsubfigure[0.2]{Electron}{Fig:ElectronGridBR} % Bottom Right
			\hspace*{\fill}
			\caption{Subfigure example of (\subref{Fig:ElectronGridTL}) an electron and (\subref{Fig:ElectronGridTR}) ...}
			\label{Fig:FourFiguresGrid}
		\end{figure}
		
		Alternatively, an uneven amount of figures can be placed as shown in \figref{Fig:ThreeFiguresTrio}.
		
		\begin{figure}[htb!]
			\centering
			\hspace*{\fill}
			\insertsubfigure[0.2]{Electron}{Fig:ElectronTrioTL} % Top Left
			\hfill
			\insertsubfigure[0.2]{Electron}{Fig:ElectronTrioTR} % Top Right
			\hspace*{\fill}
			\\[1ex] % New Row
			\insertsubfigure[0.2]{Electron}{Fig:ElectronTrioBC} % Bottom Centre
			\caption{Subfigure example of (\subref{Fig:ElectronTrioTL}) an electron and (\subref{Fig:ElectronTrioTR}) ...}
			\label{Fig:ThreeFiguresTrio}
		\end{figure}
		
%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------
\section{Inserting a Figure Place-holder}
	If you require a figure to be placed in the document but you don't have the file ready, then a place-holder can be inserted in stead using the following code:\\
	\displaycoderaw{\missingfigure{Missing figure caption}}\\
	This results in the following figure:
	
	\begin{figure}[htb!]
		\centering
		\missingfigure{Figure of a rocket or something cool}
		\caption{Example of a missing figure}
		\label{Fig:MissingFig}
	\end{figure}
	
